# [dotfiles-alacritty](https://gitlab.com/eidoom/dotfiles-alacritty)

[Companion post](https://computing-blog.netlify.app/post/alacritty/)

Fullscreen with `F11` depends on:
* `wmctrl`
* `xdotool`
